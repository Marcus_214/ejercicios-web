--

select count(*) as 'numero total'
from fullcsv

-- 

select distinct platform
from fullcsv;

-- 

select count(distinct platform)
from fullcsv;

-- 

select sum(global_sales)
from fullcsv
where platform="psp";

-- 

select sum(eu_sales+na_sales+jp_sales)
from fullcsv;

-- 

select sum(na_sales)/325.7 as 'venta_per_capita', sum(eu_sales)/741.4 as 'venta_per_capita', sum(jp_sales)/126.8 as 'venta_per_capita'
from fullcsv;

-- 

select sum(na_sales) as 'venta_na', sum(eu_sales) as 'venta_eu', sum(jp_sales) as 'venta_jp'
from fullcsv
where genre in("racing","sports","simulation");


-- 


select sum(global_sales) as 'ventas por año', year_of_release as 'año'
from fullcsv where year_of_release >= 2008 and year_of_release <= 2010
group by year_of_release;

-- 

select platform as 'plataforma', sum(global_sales) as 'ventas_por_anio', year_of_release as 'anio' 
from fullcsv 
where year_of_release >= 2008 and year_of_release <= 2010 
group by plataforma, anio
order by anio, plataforma asc;

--

select sum(global_sales) as 'ventas_totales', year_of_release as 'anio_lanzamiento' 
from fullcsv 
where genre='sports'
group by anio_lanzamiento
order by anio_lanzamiento asc;

--

select sum(global_sales) as 'ventas_totales', year_of_release as 'anio_lanzamiento' 
from fullcsv 
where genre='strategy'
group by anio_lanzamiento
order by anio_lanzamiento asc;

--

select sum(eu_sales) as 'europa', sum(na_sales) 'america', sum(jp_sales) 'japon', genre
from fullcsv
where genre in('sports', 'racing', 'simulation')
group by genre
order by genre;

--

select year_of_release as 'anio', name, global_sales
from fullcsv
where name like '%pokemon%'
group by anio
order by global_sales desc
limit 1;