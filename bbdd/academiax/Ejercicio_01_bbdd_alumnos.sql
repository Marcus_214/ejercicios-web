SELECT o.modelo, o.marca, o.numero, a.nombre, c.descripcion
FROM ordenadores o JOIN matricula m ON (o.idordenadores = m.ordenadores_idordenadores)
JOIN alumnos a ON (m.alumnos_idalumnos = a.idalumnos)
JOIN ediciones e ON (m.ediciones_idediciones = e.idediciones)
JOIN cursos c ON (c.idcursos = e.cursos_idcursos)