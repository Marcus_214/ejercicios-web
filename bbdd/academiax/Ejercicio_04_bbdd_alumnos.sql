SELECT c.descripcion, e.finicio, COUNT(a.nombre)
FROM ordenadores o JOIN matricula m ON (o.idordenadores = m.ordenadores_idordenadores)
JOIN alumnos a ON (m.alumnos_idalumnos = a.idalumnos)
JOIN ediciones e ON (m.ediciones_idediciones = e.idediciones)
JOIN cursos c ON (c.idcursos = e.cursos_idcursos)
GROUP BY e.idediciones
ORDER BY numero DESC