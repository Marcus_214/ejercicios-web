SELECT p.nombre nombre_profe, a.nombre nombre_alumno FROM profesores p
JOIN ediciones e ON (p.idprofesores = e.profesores_idprofesores)
JOIN matricula m ON (e.idediciones = m.ediciones_idediciones)
JOIN alumnos a ON (m.alumnos_idalumnos = a.idalumnos);