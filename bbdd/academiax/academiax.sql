CREATE DATABASE  IF NOT EXISTS `academiax2` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `academiax2`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: academiax2
-- ------------------------------------------------------
-- Server version	5.6.35

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alumnos`
--

DROP TABLE IF EXISTS `alumnos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alumnos` (
  `idalumnos` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idalumnos`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `asistencia`
--

DROP TABLE IF EXISTS `asistencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asistencia` (
  `idasistencia` int(11) NOT NULL AUTO_INCREMENT,
  `asisitido` varchar(45) DEFAULT NULL,
  `matricula_idmatricula` int(11) NOT NULL,
  `calendario_idcalendario` int(11) NOT NULL,
  PRIMARY KEY (`idasistencia`),
  KEY `fk_asistencia_matricula1_idx` (`matricula_idmatricula`),
  KEY `fk_asistencia_calendario1_idx` (`calendario_idcalendario`),
  CONSTRAINT `fk_asistencia_calendario1` FOREIGN KEY (`calendario_idcalendario`) REFERENCES `calendario` (`idcalendario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_asistencia_matricula1` FOREIGN KEY (`matricula_idmatricula`) REFERENCES `matricula` (`idmatricula`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `calendario`
--

DROP TABLE IF EXISTS `calendario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendario` (
  `idcalendario` int(11) NOT NULL AUTO_INCREMENT,
  `dia_lectivo` date DEFAULT NULL,
  `ediciones_idediciones` int(11) NOT NULL,
  PRIMARY KEY (`idcalendario`),
  KEY `fk_calendario_ediciones1_idx` (`ediciones_idediciones`),
  CONSTRAINT `fk_calendario_ediciones1` FOREIGN KEY (`ediciones_idediciones`) REFERENCES `ediciones` (`idediciones`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `calificaciones`
--

DROP TABLE IF EXISTS `calificaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calificaciones` (
  `idcalificaciones` int(11) NOT NULL AUTO_INCREMENT,
  `nota` int(11) DEFAULT NULL,
  `comentario` varchar(45) DEFAULT NULL,
  `examenes_idexamenes` int(11) NOT NULL,
  `matricula_idmatricula` int(11) NOT NULL,
  PRIMARY KEY (`idcalificaciones`),
  KEY `fk_calificaciones_examenes1_idx` (`examenes_idexamenes`),
  KEY `fk_calificaciones_matricula1_idx` (`matricula_idmatricula`),
  CONSTRAINT `fk_calificaciones_examenes1` FOREIGN KEY (`examenes_idexamenes`) REFERENCES `examenes` (`idexamenes`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_calificaciones_matricula1` FOREIGN KEY (`matricula_idmatricula`) REFERENCES `matricula` (`idmatricula`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cursos`
--

DROP TABLE IF EXISTS `cursos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cursos` (
  `idcursos` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) DEFAULT NULL,
  `lenguaje` varchar(45) DEFAULT NULL,
  `horas` int(11) DEFAULT NULL,
  PRIMARY KEY (`idcursos`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ediciones`
--

DROP TABLE IF EXISTS `ediciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ediciones` (
  `idediciones` int(11) NOT NULL AUTO_INCREMENT,
  `matitarda` tinyint(4) DEFAULT NULL,
  `finicio` date DEFAULT NULL,
  `ffinal` date DEFAULT NULL,
  `cursos_idcursos` int(11) NOT NULL,
  `profesores_idprofesores` int(11) NOT NULL,
  `tutor_idtutor` int(11) NOT NULL,
  PRIMARY KEY (`idediciones`),
  KEY `fk_ediciones_cursos_idx` (`cursos_idcursos`),
  KEY `fk_ediciones_profesores1_idx` (`profesores_idprofesores`),
  KEY `fk_ediciones_tutor1_idx` (`tutor_idtutor`),
  CONSTRAINT `fk_ediciones_cursos` FOREIGN KEY (`cursos_idcursos`) REFERENCES `cursos` (`idcursos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ediciones_profesores1` FOREIGN KEY (`profesores_idprofesores`) REFERENCES `profesores` (`idprofesores`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ediciones_tutor1` FOREIGN KEY (`tutor_idtutor`) REFERENCES `tutor` (`idtutor`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `examenes`
--

DROP TABLE IF EXISTS `examenes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `examenes` (
  `idexamenes` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date DEFAULT NULL,
  `tema` varchar(45) DEFAULT NULL,
  `ediciones_idediciones` int(11) NOT NULL,
  PRIMARY KEY (`idexamenes`),
  KEY `fk_examenes_ediciones1_idx` (`ediciones_idediciones`),
  CONSTRAINT `fk_examenes_ediciones1` FOREIGN KEY (`ediciones_idediciones`) REFERENCES `ediciones` (`idediciones`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `matricula`
--

DROP TABLE IF EXISTS `matricula`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matricula` (
  `idmatricula` int(11) NOT NULL AUTO_INCREMENT,
  `ediciones_idediciones` int(11) NOT NULL,
  `alumnos_idalumnos` int(11) NOT NULL,
  `ordenadores_idordenadores` int(11) NOT NULL,
  PRIMARY KEY (`idmatricula`),
  KEY `fk_matricula_ediciones1_idx` (`ediciones_idediciones`),
  KEY `fk_matricula_alumnos1_idx` (`alumnos_idalumnos`),
  KEY `fk_matricula_ordenadores1_idx` (`ordenadores_idordenadores`),
  CONSTRAINT `fk_matricula_alumnos1` FOREIGN KEY (`alumnos_idalumnos`) REFERENCES `alumnos` (`idalumnos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_matricula_ediciones1` FOREIGN KEY (`ediciones_idediciones`) REFERENCES `ediciones` (`idediciones`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_matricula_ordenadores1` FOREIGN KEY (`ordenadores_idordenadores`) REFERENCES `ordenadores` (`idordenadores`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ordenadores`
--

DROP TABLE IF EXISTS `ordenadores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordenadores` (
  `idordenadores` int(11) NOT NULL AUTO_INCREMENT,
  `modelo` varchar(45) DEFAULT NULL,
  `marca` varchar(45) DEFAULT NULL,
  `anyo_compra` varchar(45) DEFAULT NULL,
  `numero` int(11) DEFAULT NULL,
  PRIMARY KEY (`idordenadores`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `profesores`
--

DROP TABLE IF EXISTS `profesores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profesores` (
  `idprofesores` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idprofesores`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tutor`
--

DROP TABLE IF EXISTS `tutor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tutor` (
  `idtutor` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idtutor`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-03-14  9:41:20
