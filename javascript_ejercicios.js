function factorial(x) {

    var result = 1;

    for (var i = 1; i < x; i++) {

        //console.log("valor de I: " + i);

        //console.log("valor de x en la iteracio " + i + " : " + x);

        result = (result * i) + result;

        //console.log("valor de result en la iteracio " + i + " : " + result);

    }

    console.log("Resultado del factorial de " + x + " : " + result);
}

var x = 10;

factorial(x);

var msAhora = Date.now();

console.log("Ara mateix en milisegons: " + msAhora);

var dataNadal = new Date(2019,12,25);

var msNadal = dataNadal.getTime();

console.log("Milisegons el 25 del 12 de 2019: " + msNadal);

